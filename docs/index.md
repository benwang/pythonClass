# Syllabus

## Course Hardware

You will need the following for this class:

- Computer with Windows, Mac OS, or Linux

## Class Structure

Each class will be broken into four parts:

- Recap
- New concepts
- Activities
- Homework

## Schedule & Meeting Time

We will meet on **Thursdays at 7:30pm CT / 8:30pm ET / Fridays at 8:30am Taiwan** and last for approximately 1 hour. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/95335628894" target="_blank">Zoom meeting</a></b>.

|  Week  |  Date  |  Topic  |
|--|--|--|
|  1 | 9/24/2020  | Python & Class Intro |
|  2 | 10/1/2020  | Arrays and Strings |
|  3 | 10/8/2020  | Sorting and Files |
|  4 | 10/15/2020 | Sorting, Algorithms and problem solving |
|  5 | 10/22/2020 | Data - CSVs, pandas, matplotlib |
|  6 | 10/29/2020 | Data analysis 2 |
|  - | 11/5/2020  | NO CLASS |
|  7 | 11/12/2020 | PyGame / Building UIs 1 (<strike>Connect 4</strike> Mancala) |
|  8 | 11/19/2020 | Mancala - Data structure + UI |
|  - | 11/26/2020 | NO CLASS - Thanksgiving |
|  9 | 12/03/2020 | Mancala - UI - Drawing board |
| 10 | 12/10/2020 | Mancala - UI - Drawing board |
| 11 | 12/17/2020 | Mancala - Player input |
| 12 |   1/7/2020 | Mancala - Player input & gameplay logic |
| 12 |  1/14/2020 | Mancala - Scoring & gameplay logic |

<br>

## Definitions

| Term | Definition |
|--|--|
| API | Application Programming Interface. The definition of the interface into a software library or tool so that a programmer can use something someone else has created. Example is the Arduino API, which provides functions such as Serial.println() or digitalWrite() to access the hardware |
| Back end | Refers to the data-processing components of an application, not **front-end**. For example, algorithms, data structures, databases, logic, etc. |
| Front end | Refers to the user-facing components of an application. See **GUI**. |
| Git | Version control software. Tool for keeping track of the history and changes to code. Git is a type of version control system (VCS) and the top-level folder of a project managed by Git is known as a "repository". Popular websites that host Git repositories are GitHub, Gitlab, and BitBucket. |
| GUI | Graphical User Interface. Refers to the graphical aspect of an application or program. Consists commonly of windows, buttons, text (labels), 2D and 3D graphics, etc. |
| IDE | Integrated Development Environment. Software that usually includes (1) text editor, (2) compiler or interpreter, and (3) debugger. Used to write, run, and debug programs that you write. |

<br>