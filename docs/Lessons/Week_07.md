# Week 7

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

November 12, 2020

## Meeting Info

Note daylight savings time has changed our meeting time by 1 hour outside the US!

We will meet on **Thursdays at 7:30pm CST / 8:30pm EST / Fridays at 9:30am Taiwan** and last for approximately 1 hour. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/95335628894" target="_blank">Zoom meeting</a></b>.

## During Class

**Objectives:**

- Many libraries out there for making graphical applications
  - Forms-based (drag-and-drop elements, like buttons, text boxes, etc):
    - PyQt
    - tkinter
    - wxPython
  - More abstract:
    - pygame - for more interactive applications, like games
- Intro to pygame, 2D graphics, coordinate systems

**Course Material:**

- Full Week 7 course materials <a href="https://gitlab.com/benwang/pythonClass/-/tree/master/Course_Materials/Week_7" target="_blank">here</a>
- Connect 4 code (in-work) <a href="https://gitlab.com/benwang/connectfour" target="_blank">here</a>
- Mancala [reference](https://www.thesprucecrafts.com/mancala-411837)
- Reference: [https://realpython.com/pygame-a-primer/](https://realpython.com/pygame-a-primer/)
- Class recording available <a href="https://1drv.ms/v/s!AoeGNqOTDvu_uEkZJhdbne9_fw38?e=7stY7u" target="_blank">here</a> (see email for password)

## Game Design

A vote - how does this sound? It's just as easy for us to build a different game if you have something in mind.

Over the next few weeks, we will build the game of Connect 4. If you aren't familiar with the game, see [here](https://www.wikihow.com/Play-Connect-4).

First, we'll work through a plan for the game together. Here are the high-level functional requirements:

- Required:
  - The board shall have dimensions of 6 rows by 7 columns
  - Display the status of the board in the graphical user interface (GUI)
  - Each player can select where to place their chip/token when it's their turn
  - The game logic shall check after every move whether a player has won the game and display a message accordingly.
- Optional:
  - The player can select whether a 2nd player is present or to use an AI player
  - When chips are placed during a player's turn, animate the falling of the chip to the bottom of the board.
  - Allow the player to determine:
    - board dimensions (default 6x7)
    - number of chips in-a-row required to win the game (default of 4)

Suggested development strategy:

- Develop a low-fidelity (LoFi) GUI first
- Develop logic to allow human players to take turns placing chips
- Develop the logic to check for 4-in-a-row
- (Optional) Add the AI to replace player 2
- Increase the fidelity (better graphics, animations, buttons, scoreboard, etc)

## In-class Examples

Today, we'll start with some basic graphics, or *front end*.

### [Activity 1] Background and Setup

Install the pygame module: `pip3 install pygame`

See [https://realpython.com/pygame-a-primer/#background-and-setup](https://realpython.com/pygame-a-primer/#background-and-setup)

### [Activity 2] Basic Pygame Program

See [https://realpython.com/pygame-a-primer/#basic-pygame-program](https://realpython.com/pygame-a-primer/#basic-pygame-program)

### [Activity 3] Drawing a board

Now modify the above so that we have:

- A window of size 1280 x 768
- Player chips/tokens/pieces of radius 40 px
- A board layout of 6 rows x 7 columns
- Chips are red or blue

Now modify it to add:

- White borders of thickness 5 between chips
- Give the chips padding of 10 px per side
- Center the board by adding padding above and to the left of the board

### [Activity 4] A Dynamic Board

Now modify the Activity 3 code so that the board is dynamically drawn.

1. Initialize a 2D array named `board` with all 0s (make it size 6 x 7). Change some of the cells to 1s and 2s. We will use:

    - 0 to represent empty
    - 1 to represent player 1 / red chips
    - 2 to represent player 2 / blue chips

2. Modify the drawing logic so that it draws cells according to the value in the board array.
