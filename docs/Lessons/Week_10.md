# Week 10

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

December 10, 2020

## Meeting Info

We will meet on **Thursdays at 7:30pm CST / 8:30pm EST / Fridays at 9:30am Taiwan** and last for approximately 1 hour. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/95335628894" target="_blank">Zoom meeting</a></b>.

## During Class

**Objectives:**

- Draw 2nd half of Mancala board
- Draw the data structure (start with showing numbers, then change to marbles)

**Course Material:**

- Full Week 10 course materials <a href="https://gitlab.com/benwang/pythonClass/-/tree/master/Course_Materials/Week_10" target="_blank">here</a>
- Mancala course materials <a href="https://gitlab.com/benwang/mancala" target="_blank">here</a>
- Pygame [reference](https://realpython.com/pygame-a-primer/)
- Mancala [reference](https://www.thesprucecrafts.com/mancala-411837)
- Class recording available <a href="https://1drv.ms/u/s!AoeGNqOTDvu_vgnjb5aRavm5r6N6?e=fOWgbP" target="_blank">here</a> (see email for password)

If you need the code from where we left off last week, download the file [here](https://gitlab.com/benwang/pythonClass/-/raw/master/Course_Materials/Week_9/mancala.py?inline=false) (right-click, click **Save As**, and name it "mancala.py")