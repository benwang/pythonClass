# Week 1

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

September 24, 2020

## Before Class

Install Python, Visual Studio Code, and Zoom.

### Installing Python:

1. Go to Google and search for "python".
2. Find the link for Python ([www.python.org](www.python.org))
3. At the top of the page, mouse over **Downloads** and click the **Python 3.8.5** button on the right side (under "Download for Windows" or "Download for Mac OS X")

    ![Download Python](img/Week_1/Python_Download_01.png)

    <br>

4. Once the download completes, run the installer.

      ![Windows python download](img/Week_1/Python_Download_02.png)

      <br>

    - On Windows, click the **python-3.8.5.exe** that was downloaded and follow the instructions in the installer. Check "**Add Python 3.8 to PATH**" to allow us to double dlick a python script to run it. Use the default settings for everything else.

        ![Windows python install](img/Week_1/Python_Install_01.png)

        <br>

    - For Mac, follow the instructions **[here](https://realpython.com/installing-python/#how-to-install-from-the-official-installer)**

### Install Visual Studio Code (VS Code):

1. Go to Google and search for "visual studio code"
2. Find the link for VS Code ([https://code.visualstudio.com/](https://code.visualstudio.com/))
3. Click the large blue "**Download for Windows/Mac**" button.
4. Install VS Code:

    - For Windows: Run the installer once it downloads. Use the defaults.
    - For Mac, follow the instructions **[here](https://code.visualstudio.com/docs/setup/mac)**

### Install the Python extension:

1. Open VS Code.
2. On the left-hand side of the window, click the **Extensions** button (see image below).

    ![VS Code Extensions Button](img/Week_1/VS_Code_Extensions_01.png)

    <br>

3. Search for "python". Click the **Install** button under the first result.

    ![VS Code Extensions Button](img/Week_1/VS_Code_Python_01.png)

    <br>

4. Once the install completes, the **Install** button should disappear and an Uninstall button should show up.

    ![VS Code Extensions Button](img/Week_1/VS_Code_Python_03.png)

    <br>

### Installing Zoom:

1. Download and run installer when you click the link for the meeting (below).

## Meeting Info

We will meet on **Thursdays at 7:30pm CT / 8:30pm ET / Fridays at 8:30am Taiwan** and last for approximately 1 hour. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/95335628894" target="_blank">Zoom meeting</a></b>.

## During Class

**Objectives:**

- Introduction to Python and what it can do
- Interpreted vs compiled language
- Write our hello world
- Introduction to data types, arrays, and strings

**Course Material:**

- Week 1 slides available <a href="https://gitlab.com/benwang/pythonClass/-/raw/master/Course_Materials/Week_1/Week_1_Slides.pdf?inline=true" target="_blank">here</a>
- Full Week 1 course materials <a href="https://gitlab.com/benwang/pythonClass/-/tree/master/Course_Materials/Week_1" target="_blank">here</a>

## In-class Examples

### [Activity 1] Hello World

Write a program that prints "Hello world!" when run.

<details>
    <summary><b>Click for solution</b></summary>

```python
print("Hello world!")
```

</details>

<br>

### [Activity 2] Calculator

Write a simple calculator program that reads 2 values from the console and adds them together.

<details>
    <summary><b>Click for solution</b></summary>

```python
# Read in values
value1 = input("Enter first number: ")
value2 = input("Enter second number: ")

# Convert to integer
value1 = int(value1)
value2 = int(value2)

# Calculate the sum. Store in new variable "sum"
sum = value1 + value2

# Print the sum
print(sum)
```

</details>

<br>