# Week 8

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

November 19, 2020

## Meeting Info

Note daylight savings time has changed our meeting time by 1 hour outside the US!

We will meet on **Thursdays at 7:30pm CST / 8:30pm EST / Fridays at 9:30am Taiwan** and last for approximately 1 hour. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/95335628894" target="_blank">Zoom meeting</a></b>.

## During Class

**Objectives:**

- Draw Mancala board

**Course Material:**

- Full Week 8 course materials <a href="https://gitlab.com/benwang/pythonClass/-/tree/master/Course_Materials/Week_8" target="_blank">here</a>
- Mancala course materials <a href="https://gitlab.com/benwang/mancala" target="_blank">here</a>
- Pygame [reference](https://realpython.com/pygame-a-primer/)
- Mancala [reference](https://www.thesprucecrafts.com/mancala-411837)
- Class recording available <a href="https://1drv.ms/v/s!AoeGNqOTDvu_uE-1KObM-cgg0E59?e=qmr57t" target="_blank">here</a> (see email for password)