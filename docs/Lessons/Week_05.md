# Week 5

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

October 22, 2020

## Meeting Info

We will meet on **Thursdays at 7:30pm CT / 8:30pm ET / Fridays at 8:30am Taiwan** and last for approximately 1 hour. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/95335628894" target="_blank">Zoom meeting</a></b>.

## During Class

**Objectives:**

- Recap on lists, indices, and sorting
- Recap on Python modules
- Introduce CSVs, pandas, matplotlib 

**Course Material:**

- Full Week 5 course materials <a href="https://gitlab.com/benwang/pythonClass/-/tree/master/Course_Materials/Week_5" target="_blank">here</a>
- Class recording available <a href="https://1drv.ms/v/s!AoeGNqOTDvu_t2e0Z1CiQdBVolRS?e=OO8LnU" target="_blank">here</a> (see email for password)

## In-class Examples

### [Activity 1] Motor Accident Data

Download the [2018 Motor Vehicle Deaths in US by State dataset](https://gitlab.com/benwang/pythonClass/-/raw/master/Course_Materials/Datasets/MotorVehicles/2018_Motor_vehicle_deaths_in_US_by_state.csv) (right-click link, Save Link As..., **make sure to save it with a .csv file extension!**)

Load the data from the CSV. Plot the number of deaths per state as a bar chart using matplotlib. Steps to do so:

1. Install the pandas and matplotlib packages. In a terminal (Open VS Code, press Ctrl + \`), type in `pip3 install pandas matplotlib`
2. Read in the dataframe, answer question 1
3. Make a bar chart of deaths by state (x-axis is state, y-axis is deaths per year)
4. Let's pivot - Set up Jupyter Notebooks in VS Code. In VS Code, hit Ctrl + Shift + P, search for "Create new blank Jupyter notebook". Save the file as a .ipynb file.

    Note: If you see a VS Code prompt about installying `ipykernel`, click **Install**

    Note: If you see a VS Code prompt about trusting the Jupyter notebook, click **Trust**

5. Copy over the previous code into the notebook
6. Complete the rest of the activity in the notebook.

Questions:

1. How many total fatal crashes were there in the US in 2018?
2. How about total number of deaths?
3. Plot the number of deaths per state as a bar chart using matplotlib.
4. Which state has the most deaths?
5. Which state is the most dangerous for a driver? (Hint: Number of accidents per resident)

    Hint: Number of accidents per resident

6. Plot the number of deaths and death rate per state

### [Activity 2] Global CO2

Read in the CO2 and GlobalTemp datasets. Plot CO2 content vs year and global temperature vs year on the same plot. Does there appear to be a relationship?