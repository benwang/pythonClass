# Week 6

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

October 29, 2020

## Meeting Info

We will meet on **Thursdays at 7:30pm CT / 8:30pm ET / Fridays at 8:30am Taiwan** and last for approximately 1 hour. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/95335628894" target="_blank">Zoom meeting</a></b>.

## During Class

**Objectives:**

- More data analysis!
- Discuss project ideas for next week (GUI / game)
- What else are you interested in? Final project idea?

**Course Material:**

- Full Week 6 course materials <a href="https://gitlab.com/benwang/pythonClass/-/tree/master/Course_Materials/Week_6" target="_blank">here</a>
- Class recording available <a href="https://1drv.ms/v/s!AoeGNqOTDvu_uEgHmqqEkpxtB4d1?e=Gcq0xS" target="_blank">here</a> (see email for password)

## In-class Examples

### [Activity 1] Global CO2

**Source of data for today:**

- CO2:
    - [NASA](https://climate.nasa.gov/vital-signs/carbon-dioxide/)
    - [UC San Diego](https://scrippsco2.ucsd.edu/data/atmospheric_co2/icecore_merged_products.html)
- Temperature : [NASA](https://climate.nasa.gov/vital-signs/global-temperature/)

**Formatted Data (CSVs):**

- CO2:
    - [CO2 1 - Right click here and Save Link As. Make sure file extension is .csv](https://gitlab.com/benwang/pythonClass/-/raw/master/Course_Materials/Week_6/CO2.csv)
    - [CO2 2 - Right click here and Save Link As. Make sure file extension is .csv](https://gitlab.com/benwang/pythonClass/-/raw/master/Course_Materials/Week_6/merged_ice_core_yearly_CO2.csv)
- Temperature: [Temp - Right click here and Save Link As. Make sure file extension is .csv](https://gitlab.com/benwang/pythonClass/-/raw/master/Course_Materials/Week_6/GlobalTemp.csv)

**Steps:**

1. Read in the CO2 and GlobalTemp datasets.
2. Plot CO2 content vs year and global temperature vs year on the same plot. Does there appear to be a relationship?
3. Use a regression to determine a mathematical function that describes both
    - Install scipy package in python. `pip3 install scipy`
4. Discussion: Answer these questions:
    - Is this conclusive?
    - What are some of the drawbacks of this analysis?
    - What is geologic time?
    - How can we mathematically say whether these two are related?
5. Plot the regression line for CO2 compared to the UCSD CO2 dataset. Does it seem representative still?
