# Week 3

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

October 8, 2020

## Before Class

Optional: Please sign up for a LeetCode account at [https://leetcode.com/](https://leetcode.com/). This will give you more problems to practice and a better overall experience.

Alternatively, I will provide skeleton code that you can use instead, though the test cases will not be as thorough as those on LeetCode.

## Meeting Info

We will meet on **Thursdays at 7:30pm CT / 8:30pm ET / Fridays at 8:30am Taiwan** and last for approximately 1 hour. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/95335628894" target="_blank">Zoom meeting</a></b>.

## During Class

**Objectives:**

- Recap on strings, arrays, indices/selection
- Talk about code structure
- Basics on loops and conditionals
- Work on some challenges!
- Sorting

**Course Material:**

- Week 3 slides available <a href="https://gitlab.com/benwang/pythonClass/-/raw/master/Course_Materials/Week_3/Week_3_Slides.pdf?inline=true" target="_blank">here</a>
- Full Week 3 course materials <a href="https://gitlab.com/benwang/pythonClass/-/tree/master/Course_Materials/Week_3" target="_blank">here</a>
- Sorting animation and algorithms: [https://www.toptal.com/developers/sorting-algorithms](https://www.toptal.com/developers/sorting-algorithms)
- Class recording available <a href="https://1drv.ms/v/s!AoeGNqOTDvu_t1gQ6rBtTbTNAV_U" target="_blank">here</a> (see email for password)

## In-class Examples

### [Activity 1] Running sum of 1d array

Now let's work on a problem together: [https://leetcode.com/problems/running-sum-of-1d-array/](https://leetcode.com/problems/running-sum-of-1d-array/)

**If you do not have a LeetCode account, you can download a Python template of this problem [here](https://gitlab.com/benwang/pythonClass/-/raw/master/Course_Materials/Week_3/1480_Running_Sum_of_1d_Array_Blank.py)** (right-click the link, click **Save link as** and save to your Week 3 code folder).

<details>
    <summary><b>Click for solution</b></summary>

```python
class Solution:
    def runningSum(self, nums):
        # Check for corner case (if empty list)
        if len(nums) == 0:
            return []
        
        # Allocate list the same size as nums
        res = [0]*len(nums)
        
        # Initialize the first sum
        res[0] = nums[0]
        
        # Compute the rest of the sums
        for i in range(1, len(nums)):
            res[i] = res[i - 1] + nums[i]
        
        # Return the result
        return res

# Example code to test the solution (Not from LeetCode)
input =          [3,1,2,10,1]
expectedOutput = [3,4,6,16,17]

# Run our code
sol = Solution()
realOutput = sol.runningSum(input)

# Check answer
if expectedOutput == realOutput:
    print("Test passed!")
else:
    print("Test failed. Got " + str(realOutput) + ". Expected " + str(expectedOutput))
```

</details>

<br>

## [Activity 1 Challenge] Running Average of 1D Array

1. Now modify the code from Activity 1 to return the average of the list.
2. Are there any corner cases that could cause your code to break?


    <details>
        <summary><b>Hint</b></summary>
    Divide by 0 is a common reason computer programs crash.
    </details>

    <br>

## [Activity 2] Sorting a List

Implement the bubble sort algorithm (or insertion sort) to sort a list of numbers. You can start with the template **[here](https://gitlab.com/benwang/pythonClass/-/raw/master/Course_Materials/Week_3/Activity_2_Sorting_a_List.py)** (right-click link, click **Save link as** and save to your Week 3 code folder).

Generate a list of 1,000 random numbers between 0 and 10,000 and time how long it takes to sort.

Now do the same with the `sorted()` function in Python (<a href="https://developers.google.com/edu/python/sorting" target="_blank">reference</a>). Which is faster?

<details>
    <summary><b>Hint</b></summary>

To measure time, you can import the <code>time</code> module and call <code>time.time()</code> before and after the code. The difference in time is the number of seconds that elapsed. <a href="https://www.techbeamers.com/python-time-functions-usage-examples/#time-time-function" target="_blank">Reference</a>.

</details>

<br>