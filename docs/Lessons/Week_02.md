# Week 2

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

October 1, 2020

## Meeting Info

We will meet on **Thursdays at 7:30pm CT / 8:30pm ET / Fridays at 8:30am Taiwan** and last for approximately 1 hour. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/95335628894" target="_blank">Zoom meeting</a></b>.

## During Class

**Objectives:**

- Recap on strings 
- Indices/selection
- Introduction to arrays (indices and selection)
- Work on some challenges!

**Course Material:**

- Week 2 slides available <a href="https://gitlab.com/benwang/pythonClass/-/raw/master/Course_Materials/Week_2/Week_2_Slides.pdf?inline=true" target="_blank">here</a>
- Full Week 2 course materials <a href="https://gitlab.com/benwang/pythonClass/-/tree/master/Course_Materials/Week_2" target="_blank">here</a>
- Class recording available <a href="https://1drv.ms/v/s!AoeGNqOTDvu_t1atd3kK8IJsV03N?e=7qligJ" target="_blank">here</a> (see email for password)

## In-class Examples

### [Activity 1] Basic Strings

Write a program that prompts the user for a word. It'll then print out the word in pig latin.

To translate to piglatin, move the first letter to the end of the word and add "ay" to the end. For example:

- "pig" = "igpay"
- "latin" = "atinlay"
- "banana" = "ananabay"
- "will" = "illway"
- "butler" = "utlerbay"
- "happy" = "appyhay"
- "duck" = "uckday"
- "me" = "emay"

Technically, there's more to the rules, but for now, we'll use this simplified version.

Source: [Wikipedia](https://en.wikipedia.org/wiki/Pig_Latin)

<details>
    <summary><b>Click for solution</b></summary>

```python
# Read in a word from the console
s = input("Please enter a word: ")

# Perform pig latin conversion
piglatin = s[1:] + s[0] + "ay"

# Print the pig latin version of the word
print(piglatin)
```

</details>

<br>

### [Activity 2] Letter count

Write a program that counts the number of occurrences of the letter "y" in a string.

Now try inputting the string: "Happy birthday to you!". Expected output is 3.

<details>
    <summary><b>Click for solution</b></summary>

```python
# Read in a phrase from the console
inputString = input("Please enter a phrase: ")

# Count the number of "y"s
numY = 0

for currChar in inputString:
    if currChar == "y":
        numY += 1           # equal to numY = numY + 1

# Print result
print(numY)

```

</details>

<br>