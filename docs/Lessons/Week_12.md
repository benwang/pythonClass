# Week 12

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

January 7, 2021

## Meeting Info

We will meet on **Thursdays at 7:30pm CST / 8:30pm EST / Fridays at 9:30am Taiwan** and last for approximately 1 hour. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/95335628894" target="_blank">Zoom meeting</a></b>.

## During Class

**Objectives:**

- Fix mouseover highlighting
- Start adding gameplay logic

**Course Material:**

- Full Week 12 course materials <a href="https://gitlab.com/benwang/pythonClass/-/tree/master/Course_Materials/Week_12" target="_blank">here</a>
- Mancala course materials <a href="https://gitlab.com/benwang/mancala" target="_blank">here</a>
- Pygame [reference](https://realpython.com/pygame-a-primer/)
- Pygame mouse [reference](https://learn.yorkcs.com/2019/10/03/pygame-basics-mouse-input/)
- Mancala [reference](https://www.thesprucecrafts.com/mancala-411837)
- Python [round function](https://www.w3schools.com/python/ref_func_round.asp)
- Python [floor function](https://www.tutorialspoint.com/python/number_floor.htm)
- Class recording available <a href="https://1drv.ms/v/s!AoeGNqOTDvu_vhpzVWPYfPXfoXFF?e=hw7mSj" target="_blank">here</a> (see email for password)

If you need the code from where we left off last week, download the file [here](https://gitlab.com/benwang/pythonClass/-/raw/master/Course_Materials/Week_11/mancala.py?inline=false) (right-click, click **Save As**, and name it "mancala.py")