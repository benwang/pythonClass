# Week 4

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

October 15, 2020

## Before Class

## Meeting Info

We will meet on **Thursdays at 7:30pm CT / 8:30pm ET / Fridays at 8:30am Taiwan** and last for approximately 1 hour. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/95335628894" target="_blank">Zoom meeting</a></b>.

## During Class

**Objectives:**

- Recap on lists, indices, and sorting
- Recap on Python modules
- Finish sorting
- Work on some challenges

**Course Material:**

- Full Week 4 course materials <a href="https://gitlab.com/benwang/pythonClass/-/tree/master/Course_Materials/Week_4" target="_blank">here</a>
- Class recording available <a href="https://1drv.ms/v/s!AoeGNqOTDvu_t2ULjSbIPj1z0sXJ" target="_blank">here</a> (see email for password)

## In-class Examples

### [Activity 1] Minimum, Maximum, and Median of List

Given a list of GPAs from a file, find the min, max, and median of the list.

Download the data file [here](https://gitlab.com/benwang/pythonClass/-/raw/master/Course_Materials/Week_4/GPAs.txt?inline=false) and put it with your Week 4 files (right-click, **Save link as**).

Now double check your work using the Python `statistics` module (<a href="https://stackabuse.com/calculating-mean-median-and-mode-in-python/#usingpythonsmedian" target="_blank">reference</a>).

### [Activity 2] Plus One

From LeetCode: <a href="https://leetcode.com/problems/plus-one/" target="_blank">https://leetcode.com/problems/plus-one/</a>

**If you do not have a LeetCode account, you can download a Python template of this problem [here](https://gitlab.com/benwang/pythonClass/-/raw/master/Course_Materials/Week_4/66_Plus_One.py)** (right-click the link, click **Save link as** and save to your Week 4 code folder).

What are the corner cases?

<details>
    <summary><b>Hint</b></summary>

What happens when you add 1 to 99?

</details>

<br>

### [Activity 3] Best Time to Buy and Sell Stock

From LeetCode: <a href="https://leetcode.com/problems/best-time-to-buy-and-sell-stock/" target="_blank">https://leetcode.com/problems/best-time-to-buy-and-sell-stock/</a>

**If you do not have a LeetCode account, you can download a Python template of this problem [here](https://gitlab.com/benwang/pythonClass/-/raw/master/Course_Materials/Week_4/121_Best_Time_to_Buy_and_Sell_Stock.py)** (right-click the link, click **Save link as** and save to your Week 4 code folder).

Start by brainstorming some approaches.

1. Brute force method
2. Can you do better?

    <details>
        <summary><b>Hint</b></summary>

    What's the best you can do? Look at each element how many times?

    </details>

    <br>

What are the corner cases?

<details>
    <summary><b>Hint</b></summary>

What if you have 2 elements? 1? 0?

</details>

<br>