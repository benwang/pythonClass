# Week 9

<!-- JS for copy code to clipboard button -->
<script src="../js/clipboard.min.js"></script>

December 3, 2020

## Meeting Info

Note daylight savings time has changed our meeting time by 1 hour outside the US!

We will meet on **Thursday at 8:00pm CST / 9:00pm EST / Friday at 10:00am Taiwan** and last for approximately 1 hour. <b>Link to the class <a href="https://wustl-hipaa.zoom.us/j/95335628894" target="_blank">Zoom meeting</a></b>.

## During Class

**Objectives:**

- Draw store for player 1
- Derive equation to translate a player's pit

**Course Material:**

- Full Week 9 course materials <a href="https://gitlab.com/benwang/pythonClass/-/tree/master/Course_Materials/Week_9" target="_blank">here</a>
- Mancala course materials <a href="https://gitlab.com/benwang/mancala" target="_blank">here</a>
- Pygame [reference](https://realpython.com/pygame-a-primer/)
- Mancala [reference](https://www.thesprucecrafts.com/mancala-411837)
- Class recording available <a href="https://1drv.ms/v/s!AoeGNqOTDvu_vgjotsMz8pRyv-dZ?e=JafICK" target="_blank">here</a> (see email for password)

If you need the code from where we left off last week, download the file [here](https://gitlab.com/benwang/pythonClass/-/raw/master/Course_Materials/Week_8/mancala.py?inline=false) (right-click, click **Save As**, and name it "mancala.py")