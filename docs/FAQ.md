# Frequently Asked Questions

### On Mac, having errors with unexpected EOF (End of File)

Make sure that you are using `python3` instead of `python` to execute your script. If Python 2 is being used instead, you can fix this by:

1. Open VS Code
2. Go to **View** > **Command Palette...**
3. Type in "Python: Select Interpreter" and click it
4. Select Python 3.8.x (probably 3.8.5). If multiple are present, select the one in `/usr/...`

[Source](https://stackoverflow.com/questions/48135624/how-can-i-change-python-version-in-visual-studio-code)

