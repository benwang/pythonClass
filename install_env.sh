#!/bin/bash

# Python version to use:
# Python 3.8 is chosen b/c that's currently the latest
python_cmd=/usr/bin/python3.8

# Check for python 3
command -v $python_cmd > /dev/null && have_python3_x=true || have_python3_x=false

if $have_python3_x; then
    echo $python_cmd command found
else
    echo Missing $python_cmd. Continuing with generic python3 command
    python_cmd=python3

    # Verify python3 available
    command -v $python_cmd > /dev/null && have_python3_x=true || have_python3_x=false

    if ! $have_python3_x; then
        echo ERROR: Python 3 is required. Python 3.8 is preferred. Please install and rerun this script.
        exit 1
    fi
fi

# Make sure virtualenv command exists
command -v virtualenv > /dev/null && have_virtualenv=true || have_virtualenv=false

if $have_virtualenv; then
    echo virtualenv command found.
else
    echo Missing virtualenv. Installing...
    $python_cmd -m pip install virtualenv

    if [ $? -ne 0 ]; then
        echo "Error installing virtualenv. Please check for any errors above or install manually."
        exit 1
    fi
fi

# Check if virtualenvironment exists
if [ ! -d venv ]; then
    echo "venv folder doesn't exist. Creating virtual environment now"
    virtualenv venv --python=$python_cmd

    if [ $? -ne 0 ]; then
        echo "Error creating virtualenv. Please check for any errors above."
        exit 1
    fi
fi

# Activate session for rest of the script
echo Activating virtual environment ./venv
source venv/bin/activate

# Install dependencies
echo Installing python packages in requirements.txt...
pip3 install -r requirements.txt

# Check for errors
if [ $? -ne 0 ]; then
    echo "Error installing Python dependencies. Please check for any errors above."
    exit 1
fi

echo Installation complete

echo ""
echo Remember to run \"source venv/bin/activate\" first!
