# Mancala

###################################
# Constants
###################################

# Window size
WINDOW_WIDTH = 1280 # px
WINDOW_HEIGHT = 720 # px

# Dimensions of each cell of board
CELL_WIDTH = WINDOW_WIDTH / 10  # px
PIT_WIDTH = 100                 # px
PIT_RADIUS = PIT_WIDTH / 2      # px

# Padding around the board
TOP_PADDING = 128   # px
LEFT_PADDING = 128  # px
# ?? bottom and right padding?

# Colors
BOARD_BACKGROUND_COLOR = (193,154,107)
PIT_COLOR = (143,114,79)

###################################
# Drawing Functions
###################################

def drawStore(x, y):
    pygame.draw.circle(screen, PIT_COLOR, (x, y - 0.5 * CELL_WIDTH), PIT_RADIUS)
    pygame.draw.circle(screen, PIT_COLOR, (x, y + 0.5 * CELL_WIDTH), PIT_RADIUS)
    pygame.draw.rect(screen, PIT_COLOR,
        (x - PIT_RADIUS, y - 0.5 * CELL_WIDTH, PIT_WIDTH, CELL_WIDTH))

def drawPlayerBoard(x, y, sign, playerBoard):
    for pitNum in range(7):
        pitX = x + sign * (1.5 + pitNum) * CELL_WIDTH

        if pitNum < 6:  # 0 1 2 3 4 5
            # Draw pit
            pitY = y + sign * 1.5 * CELL_WIDTH
            pygame.draw.circle(screen, PIT_COLOR, (pitX, pitY), PIT_RADIUS)
        else:           # 6
            # Draw store
            drawStore(pitX, y + sign * CELL_WIDTH)

        # Draw number of stones in this pit or store
        pitText = Font.render(str(playerBoard[pitNum]), False, (0, 0, 0), PIT_COLOR)
        screen.blit(pitText, (pitX, pitY))

###################################
# Game Logic
###################################

# Initialize board
#               ===== pits =====  store
#               0  1  2  3  4  5  6
player1Board = [4, 4, 4, 4, 4, 4, 3]
player2Board = [4, 4, 1, 4, 4, 4, 0]

# Import and initialize the pygame library
import pygame
pygame.init()

Font = pygame.font.SysFont('timesnewroman', 30)

# Set up the drawing window
screen = pygame.display.set_mode([WINDOW_WIDTH, WINDOW_HEIGHT])

# Run until the user asks to quit
running = True
while running:

    # Did the user click the window close button?
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # Fill the background with white
    screen.fill((255, 255, 255))

    # Draw the board rectangle
    pygame.draw.rect(screen, BOARD_BACKGROUND_COLOR,
        (LEFT_PADDING, TOP_PADDING, 8 * CELL_WIDTH, 2 * CELL_WIDTH))

    # Draw player 1's board
    drawPlayerBoard(LEFT_PADDING, TOP_PADDING, 1, player1Board)

    # Draw player 2's board
    drawPlayerBoard(LEFT_PADDING + 8 * CELL_WIDTH, TOP_PADDING + 2 * CELL_WIDTH, -1, player2Board)

    # Flip the display
    pygame.display.flip()

# Done! Time to quit.
pygame.quit()