# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
# 2018 Motor Vehicle Deaths

# Questions:
# 1. How many total fatal crashes were there in the US in 2018?
# 2. How about total number of deaths?
# 3. Plot the number of deaths per state as a bar chart using matplotlib.
# 4. Which state has the most deaths?
# 5. Which state is the most dangerous for a driver? (Hint: Number of accidents per resident)
# 6. Plot the number of deaths and death rate per state


# %%

import pandas
import matplotlib.pyplot as plt

# Read in CSV as a dataframe
# Reference: https://realpython.com/python-csv/#parsing-csv-files-with-the-pandas-library
df = pandas.read_csv('2018_Motor_vehicle_deaths_in_US_by_state.csv')
print(df)


# %%
# You can access elements like this:
print(df['State'][0])


# %%
# Or get a whole column:
print(df['State'])


# %%
# 1. How many total fatal crashes were there in the US in 2018?
print("Total number of fatal crashes: " + str(sum(df['Fatal crashes'])))

# 2. How about total number of deaths?
print("Total number of deaths: " + str(sum(df['Deaths'])))


# %%
# 3. Plot the number of deaths per state as a bar chart using matplotlib.
# Reference: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.bar.html
ax = df.plot.bar(x='State', y='Deaths')
plt.show()


# %%
# 4. Which state has the most deaths?
maxDeathIdx = df['Deaths'].idxmax()
print("State with most crashes: " + str(df['State'][maxDeathIdx]))


# %%
# 5. Which state is the most dangerous for a driver? (Hint: Number of accidents per resident)
maxDeathIdx = df['Deaths per 100,000 population'].idxmax()
print("State with most crashes: " + str(df['State'][maxDeathIdx]))


# %%
# 6. Plot the number of deaths and death rate per state
# Reference: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.bar.html
# Selecting multiple columns: https://datagy.io/pandas-select-columns/
# Plotting multiple y-axes: https://stackoverflow.com/questions/24183101/pandas-bar-plot-with-two-bars-and-two-y-axis
ax2 = df[['State', 'Deaths', 'Deaths per 100,000 population']].plot.bar(x='State', secondary_y='Deaths per 100,000 population')
plt.show()


