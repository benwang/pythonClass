# Mancala

import pygame
import math

###################################
# Constants
###################################

# Window size
WINDOW_WIDTH = 1280 # px
WINDOW_HEIGHT = 720 # px

# Dimensions of each cell of board
CELL_WIDTH = WINDOW_WIDTH / 10  # px
PIT_WIDTH = 100                 # px
PIT_RADIUS = PIT_WIDTH / 2      # px

# Padding around the board
TOP_PADDING = 128   # px
LEFT_PADDING = 128  # px
# ?? bottom and right padding?

# Colors
BOARD_BACKGROUND_COLOR = (193,154,107)
PIT_COLOR = (143,114,79)
SELECTED_PIT_COLOR = (163,134,99)

###################################
# Drawing Functions
###################################

def drawPlayerLabel():
    # Draw number of stones in this pit or store
    playerText = Font.render("Player " + str(playerTurn) + "'s turn",
        False, (0, 0, 0), (255, 255, 255))
    screen.blit(playerText, (WINDOW_WIDTH / 2, WINDOW_HEIGHT - 50))

def drawStore(x, y, pitColor):
    pygame.draw.circle(screen, pitColor, (x, y - 0.5 * CELL_WIDTH), PIT_RADIUS)
    pygame.draw.circle(screen, pitColor, (x, y + 0.5 * CELL_WIDTH), PIT_RADIUS)
    pygame.draw.rect(screen, pitColor,
        (x - PIT_RADIUS, y - 0.5 * CELL_WIDTH, PIT_WIDTH, CELL_WIDTH))

def drawPlayerBoard(x, y, sign, playerBoard, selectedPitNum):
    for pitNum in range(7):
        pitColor = PIT_COLOR
        if pitNum == selectedPitNum and pitNum < 6:
            pitColor = SELECTED_PIT_COLOR

        pitX = x + sign * (1.5 + pitNum) * CELL_WIDTH

        if pitNum < 6:  # 0 1 2 3 4 5
            # Draw pit
            pitY = y + sign * 1.5 * CELL_WIDTH
            pygame.draw.circle(screen, pitColor, (pitX, pitY), PIT_RADIUS)
        else:           # 6
            # Draw store
            drawStore(pitX, y + sign * CELL_WIDTH, pitColor)

        # Draw number of stones in this pit or store
        pitText = Font.render(str(playerBoard[pitNum]), False, (0, 0, 0), pitColor)
        screen.blit(pitText, (pitX, pitY))

###################################
# Game Logic
###################################

# Given coordinates of pit that was clicked, 
# modify the board per mancala rules.
def processClick(mousePitX, mousePitY):
    global playerTurn
    
    # If click was out-of-bounds, return immediately
    if mousePitY < 0 or mousePitY > 1 or mousePitX < 0 or mousePitX > 5:
        return
    
    # Must be correct player's turn
    if mousePitY == 1 and playerTurn != 1 or mousePitY == 0 and playerTurn != 2:
        return

    if mousePitY == 1:
        # Player 1's pit
        whoseTurn = 1                   # Player 1
        currPit = mousePitX             # Starting pit number
        numLeft = player1Board[currPit] # Number in that pit
        currSide = 1                    # Player 1's side first

        player1Board[currPit] = 0
    else:
        # Player 2's pit
        whoseTurn = 2                   # Player 2
        currPit = 5 - mousePitX         # Starting pit number
        numLeft = player2Board[currPit] # Number in that pit
        currSide = 2                    # Player 2's side first

        player2Board[currPit] = 0

    idx = currPit + 1
    while numLeft > 0:
        if currSide == 1:
            while (
                numLeft > 0 and (
                    (whoseTurn == 1 and idx <= 6) or
                    (whoseTurn == 2 and idx <= 5)
                )
            ):
                numLeft -= 1
                player1Board[idx] += 1
                idx += 1
            
            currSide = 2
            idx = 0
        else:
            while (
                numLeft > 0 and (
                    (whoseTurn == 2 and idx <= 6) or
                    (whoseTurn == 1 and idx <= 5)
                )
            ):
                numLeft -= 1
                player2Board[idx] += 1
                idx += 1
            
            currSide = 1
            idx = 0
    
    # Swap player turn
    if playerTurn == 1:
        playerTurn = 2
    else:
        playerTurn = 1



# Initialize board
#               ===== pits =====  store
#               0  1  2  3  4  5  6
player1Board = [4, 4, 4, 4, 4, 4, 0]
player2Board = [4, 4, 4, 4, 4, 4, 0]

# Player 1 always starts
playerTurn = 1

# Initialize the pygame library
pygame.init()

Font = pygame.font.SysFont('timesnewroman', 30)

# Set up the drawing window
screen = pygame.display.set_mode([WINDOW_WIDTH, WINDOW_HEIGHT])

# Run until the user asks to quit
running = True
while running:

    # Get mouse coordinates
    mouseX,mouseY = pygame.mouse.get_pos()
    mousePitX = round((mouseX - LEFT_PADDING) / CELL_WIDTH - 1.5)
    mousePitY = math.floor(mouseY / CELL_WIDTH - TOP_PADDING / CELL_WIDTH)
    print((mousePitX, mousePitY))

    # Check for input
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.MOUSEBUTTONUP:
            print("Mouse click detected.")
            processClick(mousePitX, mousePitY)

    # Fill the background with white
    screen.fill((255, 255, 255))

    # Draw player turn
    drawPlayerLabel()

    # Draw the board rectangle
    pygame.draw.rect(screen, BOARD_BACKGROUND_COLOR,
        (LEFT_PADDING, TOP_PADDING, 8 * CELL_WIDTH, 2 * CELL_WIDTH))

    # Draw player 1's board
    if mousePitY == 1 and playerTurn == 1:
        drawPlayerBoard(LEFT_PADDING, TOP_PADDING, 1, player1Board, mousePitX)
    else:
        drawPlayerBoard(LEFT_PADDING, TOP_PADDING, 1, player1Board, -1)

    # Draw player 2's board
    if mousePitY == 0 and playerTurn == 2:
        drawPlayerBoard(LEFT_PADDING + 8 * CELL_WIDTH, TOP_PADDING + 2 * CELL_WIDTH, -1, player2Board, 5 - mousePitX)
    else:
        drawPlayerBoard(LEFT_PADDING + 8 * CELL_WIDTH, TOP_PADDING + 2 * CELL_WIDTH, -1, player2Board, -1)

    # Flip the display
    pygame.display.flip()

# Done! Time to quit.
pygame.quit()