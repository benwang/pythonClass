# Read in a phrase from the console
inputString = input("Please enter a phrase: ")

# Count the number of "y"s
numY = 0

for currChar in inputString:
    if currChar == "y":
        numY += 1           # equal to numY = numY + 1

# Print result
print(numY)
