class Solution:
    def runningSum(self, nums):
        # Check for corner case (if empty list)
        if len(nums) == 0:
            return []
        
        # Allocate list the same size as nums
        res = [0]*len(nums)
        
        # Initialize the first sum
        res[0] = nums[0]
        
        # Compute the rest of the sums
        for i in range(1, len(nums)):
            res[i] = res[i - 1] + nums[i]
        
        # Return the result
        return res

# Example code to test the solution (Not from LeetCode)
input =          [3,1,2,10,1]
expectedOutput = [3,4,6,16,17]

# Run our code
sol = Solution()
realOutput = sol.runningSum(input)

# Check answer
if expectedOutput == realOutput:
    print("Test passed!")
else:
    print("Test failed. Got " + str(realOutput) + ". Expected " + str(expectedOutput))
