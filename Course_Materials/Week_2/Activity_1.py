# Read in a word from the console
s = input("Please enter a word: ")

# Perform pig latin conversion
piglatin = s[1:] + s[0] + "ay"

# Print the pig latin version of the word
print(piglatin)