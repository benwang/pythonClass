# Read in a word from the console
s = input("Please enter a sentence: ")

# Perform pig latin conversion
splitWords = s.split(" ")
pigLatin = ""

for i in range(len(splitWords)):
    word = splitWords[i]
    pigLatin += word[1:] + word[0] + "ay "

# Print the pig latin version of the word
print(pigLatin)