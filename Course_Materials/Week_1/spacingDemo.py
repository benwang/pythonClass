# Example Python script demonstrating spacing

# Ask for user's name
print("Hello there!")
username = input("Enter your name: ")

# Print something based on it
if username == "Ben":
    # Notice the tab used here
    print("Hey, you wrote me!")
else:
    print("Hi " + username + "!")
    print("This is printed from another print() command in the same else block.")
    print("Tabs let you group lines of code together logically.")

print("Goodbye!")