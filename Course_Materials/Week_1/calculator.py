## Attempt 1

# Read in values
value1 = input("Enter first number: ")
value2 = input("Enter second number: ")

# Calculate the sum
sum = value1 + value2

# Print the sum
print(sum)

## Attempt 2

# Read in values
value1 = input("Enter first number: ")
value2 = input("Enter second number: ")

# Convert to integer
value1 = int(value1)
value2 = int(value2)

# Calculate the sum. Store in new variable "sum"
sum = value1 + value2

# Print the sum
print(sum)

## Attempt 3 - Now with division

# Read in values
value1 = input("Enter first number: ")
value2 = input("Enter second number: ")

# Convert to integer
value1 = int(value1)
value2 = int(value2)

# Calculate the sum. Store in new variable "sum"
quotient = value1 /value2

# Print the sum
print(quotient)

## Attempt 3 - Now with floating point division

# Read in values
value1 = input("Enter first number: ")
value2 = input("Enter second number: ")

# Convert to integer
value1 = float(value1)
value2 = float(value2)

# Calculate the sum. Store in new variable "sum"
quotient = value1 /value2

# Print the sum
print(quotient)


