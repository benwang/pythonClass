"""
66. Plus One
Source: https://leetcode.com/problems/plus-one/

Given a non-empty array of digits representing a non-negative integer, increment one to the integer.

The digits are stored such that the most significant digit is at the head of the list, and each element in the array contains a single digit.

You may assume the integer does not contain any leading zero, except the number 0 itself.

 

Example 1:

Input: digits = [1,2,3]
Output: [1,2,4]
Explanation: The array represents the integer 123.
Example 2:

Input: digits = [4,3,2,1]
Output: [4,3,2,2]
Explanation: The array represents the integer 4321.
Example 3:

Input: digits = [0]
Output: [1]
 

Constraints:

1 <= digits.length <= 100
0 <= digits[i] <= 9

"""

class Solution(object):
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        
        # Increment last digit by 1
        digits[-1] += 1
        
        # Check for overflow
        index = -1
        while index > -len(digits) and digits[index] >= 10:
            digits[index] -= 10
            digits[index - 1] += 1
            index -= 1
        
        # Special case that first digit overflows
        if digits[0] >= 10:
            digits[0] -= 10
            digits = [1] + digits
        
        return digits
        




########################################################################
# Do not change the code below (except for the input and expectedOutput)
########################################################################

# Example code to test the solution (Not from LeetCode)

import time

# Counter
numTests = 0

def checkAnswer(testInput, expectedOutput):
    global numTests

    numTests += 1

    # Run our code
    sol = Solution()
    realOutput = sol.plusOne(testInput)

    # Check answer
    if expectedOutput == realOutput:
        print("Test passed!")
    else:
        print("Test failed. Got " + str(realOutput) + ". Expected " + str(expectedOutput))
        print("Please try again.")
        exit()

startTime = time.time()
checkAnswer([1,2,3], [1,2,4])
checkAnswer([4,3,2,1], [4,3,2,2])
checkAnswer([0], [1])
checkAnswer([9], [1,0])
checkAnswer([9,9,9,9], [1,0,0,0,0])
endTime = time.time()

print("You passed all " + str(numTests) + " tests in " + str(endTime - startTime) + " seconds!")