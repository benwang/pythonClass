import random

NUM_STUDENTS = 70

# Generate student data
GPAs = []
for i in range(NUM_STUDENTS):
    GPAs.append(random.random() * 4.0)

# Write data to file
f = open("GPAs.txt", "w")
for i in GPAs:
    # Print GPA formatted to 2 decimal points
    f.write(str("{0:.2f}".format(i)) + "\n")
f.close()

print("Complete.")