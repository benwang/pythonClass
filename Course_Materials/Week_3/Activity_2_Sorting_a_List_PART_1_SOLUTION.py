# Sorting a List
# Implement the bubble sort algorithm (or insertion sort) to sort a list of numbers.
# Generate a list of 1,000 random numbers between 0 and 10,000 and time how long it takes to sort.
# Now do the same with the `sorted()` function in Python. Which is faster?

# Import modules here
import random

# Parameters
MAX      = 10000
NUM_ELTS = 10

# Generate random data
data = []
for i in range(0, NUM_ELTS):
    data.append(random.randint(0, MAX))

# The above can also be done in 1 line with: data = random.sample(range(0, MAX), NUM_ELTS)

# Power of Python: You can print anything
print("Generated dataset: " + str(data))

# Define bubble sort here as a function
"""
Bubble sort algorithm is: https://www.toptal.com/developers/sorting-algorithms/bubble-sort

for i = 1:n,
    swapped = false
    for j = n:i+1,
        if a[j] < a[j-1],
            swap a[j,j-1]
            swapped = true
    → invariant: a[1..i] in final position
    break if not swapped
end
"""
def bubbleSort(a):
    for i in range(0, len(a)):
        swapped = False

        for j in range(len(a) - 1, i, -1):
            # print("i:" + str(i) + " j:" + str(j))
            if a[j] < a[j - 1]:
                temp = a[j]
                a[j] = a[j - 1]
                a[j - 1] = temp
                swapped = True

        if swapped == False:
            break

# Make a copy of the data
bubbleSortedData = data[:]

# Sort the data using bubble sort
bubbleSort(bubbleSortedData)

print("Sorted data: " + str(bubbleSortedData))