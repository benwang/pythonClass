# [Activity 3] Minimum, Maximum, and Median of List
# Given a list of GPAs from a file, find the min, max, and median of the list.

# Read in data file
f = open("GPAs.txt", "r")
data = f.read()
f.close()

# print(data)

# Separate input
dataSplit = data.split()
print(dataSplit)

# Convert list of strings into list of floats
GPAs = []
for GPA_String in dataSplit:
    GPAs.append(float(GPA_String))
print(GPAs)

# Might be easier if we sorted our list
sortedGPAs = sorted(GPAs)
print("Min GPA:  " + str(sortedGPAs[0]))
print("Max GPA:  " + str(sortedGPAs[-1]))

lowerIndex = int(len(sortedGPAs) / 2) - 1
if len(sortedGPAs) % 2 == 0:
    # Even number of elements. Median is average of middle 2
    median = (sortedGPAs[lowerIndex] + sortedGPAs[lowerIndex + 1]) / 2
else:
    median = sortedGPAs[lowerIndex]

print("Median GPA: " + str(median))

# Double check our answer
import statistics
print("=== Answers using statistics module ===")
print("Min GPA:  " + str(min(GPAs)))
print("Max GPA:  " + str(max(GPAs)))
print("Actual median GPA: " + str(statistics.median(GPAs)))

# Ah, packages are nice, huh?