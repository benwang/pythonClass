# Sorting a List
# Implement the bubble sort algorithm (or insertion sort) to sort a list of numbers.
# Generate a list of 1,000 random numbers between 0 and 10,000 and time how long it takes to sort.
# Now do the same with the `sorted()` function in Python. Which is faster?

# Import modules here
# import random

# Parameters
MAX      = 10000
NUM_ELTS = 10

# Generate random data
data = []
# YOUR CODE HERE

# Power of Python: You can print anything
print("Generated dataset: " + str(data))

# Define bubble sort here as a function
"""
Bubble sort algorithm is: https://www.toptal.com/developers/sorting-algorithms/bubble-sort

for i = 1:n,
    swapped = false
    for j = n:i+1,
        if a[j] < a[j-1],
            swap a[j,j-1]
            swapped = true
    → invariant: a[1..i] in final position
    break if not swapped
end
"""
def bubbleSort(a):
    # YOUR CODE HERE

# Make a copy of the data
bubbleSortedData = data[:]

# Sort the data using bubble sort
bubbleSort(bubbleSortedData)

print("Sorted data: " + str(bubbleSortedData))