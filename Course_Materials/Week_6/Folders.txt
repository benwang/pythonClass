Folders
Files

<TOP LEVEL FOLDER>
        Windows: C:\
        Mac:     /
| ...
| Folder 0
  | File A
  | Folder1
    | File 1
    | File 2

        vv User/Home directory
/Users/sageusername/Downloads
                    ../CO2.ipynb
                    ../CO2.csv
                    ../<other files>

C:\Users\CJ\Downloads\
C:\Users\CJ\Documents\
                ..\PythonClass
                        ..\Week_6\
                            ..\CO2.csv
                            ..\CO2.ipynb
                            ..\...