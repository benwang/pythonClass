# Mancala

###################################
# Constants
###################################

# Window size
WINDOW_WIDTH = 1280 # px
WINDOW_HEIGHT = 720 # px

# Dimensions of each cell of board
CELL_WIDTH = WINDOW_WIDTH / 10  # px
PIT_WIDTH = 100                 # px
PIT_RADIUS = PIT_WIDTH / 2      # px

# Padding around the board
TOP_PADDING = 128   # px
LEFT_PADDING = 128  # px
# ?? bottom and right padding?

###################################
# Game Logic
###################################

# Import and initialize the pygame library
import pygame
pygame.init()

# Set up the drawing window
screen = pygame.display.set_mode([WINDOW_WIDTH, WINDOW_HEIGHT])

# Run until the user asks to quit
running = True
while running:

    # Did the user click the window close button?
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # Fill the background with white
    screen.fill((255, 255, 255))

    # Draw the board rectangle
    pygame.draw.rect(screen, (20, 20, 20),
        (LEFT_PADDING, TOP_PADDING, 8 * CELL_WIDTH, 2 * CELL_WIDTH))

    # Draw a solid blue circle in the center
    for pitNum in range(6):
        pitX = LEFT_PADDING + (1.5 + pitNum) * CELL_WIDTH
        pitY = TOP_PADDING + 1.5 * CELL_WIDTH
        pygame.draw.circle(screen, (0, 0, 255), (pitX, pitY), PIT_RADIUS)

    # Flip the display
    pygame.display.flip()

# Done! Time to quit.
pygame.quit()