#!/bin/bash

echo Installing Python 3 pip
sudo apt install -y python3 python3-pip

echo Running install script
python3 -m pip install virtualenv

python3 -m virtualenv venv

source venv/bin/activate

# Install dependencies
echo Installing python packages in requirements.txt...
pip3 install -r requirements.txt

# Check for errors
if [ $? -ne 0 ]; then
    echo "Error installing Python dependencies. Please check for any errors above."
    exit 1
fi

echo Installation complete

echo ""
echo Remember to run \"source venv/bin/activate\" first!


echo Done
